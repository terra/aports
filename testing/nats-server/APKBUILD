# Contributor: Henrik Riomar <henrik.riomar@gmail.com>
# Maintainer: Henrik Riomar <henrik.riomar@gmail.com>
pkgname=nats-server
pkgver=2.9.2
pkgrel=0
pkgdesc="High-Performance server for NATS.io"
url="https://github.com/nats-io/nats-server"
arch="all !x86 !armv7 !armhf !s390x !ppc64le" # limited by failing check()
options="!check" # unstable
license="Apache-2.0"
makedepends="go"
source="https://github.com/nats-io/nats-server/archive/v$pkgver/nats-server-$pkgver.tar.gz"

export GOFLAGS="$GOFLAGS -modcacherw"
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build
}

check() {
	go test -v -p=1 -run=TestNoRace ./server -count=1 -vet=off -timeout=30m -failfast
}

package() {
	install -Dm755 nats-server "$pkgdir"/usr/bin/nats-server
}

sha512sums="
57571f127b34ebfee326fcf230a78656de649ed54c46b741f429d591dd231d3f534ed95cb219a5b3a4fb54508a0ad953425a10026150252b28f8c69bd8eeef5b  nats-server-2.9.2.tar.gz
"
