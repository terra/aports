# Maintainer: psykose <alice@ayaya.dev>
pkgname=hwdata
pkgver=0.362
pkgrel=0
pkgdesc="Hardware identification and configuration data"
url="https://github.com/vcrhonek/hwdata"
arch="noarch"
license="GPL-2.0-or-later"
subpackages="$pkgname-dev $pkgname-usb $pkgname-pci $pkgname-pnp $pkgname-net"
source="$pkgname-$pkgver.tar.gz::https://github.com/vcrhonek/hwdata/archive/refs/tags/v$pkgver.tar.gz
	install-T.patch
	"

replaces="hwids"
# be higher since hwids was a large date version
provides="hwids=20220101-r$pkgrel"

build() {
	./configure \
		--prefix=/usr \
		--sysconfdir=/etc \
		--datadir=/usr/share
}

package() {
	depends="$pkgname-usb $pkgname-pci $pkgname-pnp $pkgname-net"
	make -j1 DESTDIR="$pkgdir" install
	# remove modprobe blacklist
	rm -r "$pkgdir"/usr/lib
}

dev() {
	default_dev
	depends="$pkgname=$pkgver-r$pkgrel"
}

usb() {
	pkgdesc="$pkgdesc (usb data)"
	provides="hwids-usb=20220101-r$pkgrel"
	replaces="hwids-usb"

	amove usr/share/hwdata/usb.ids
}

pci() {
	pkgdesc="$pkgdesc (pci data)"
	provides="hwids-pci=20220101-r$pkgrel"
	replaces="hwids-pci"

	amove usr/share/hwdata/pci.ids
}

net() {
	pkgdesc="$pkgdesc (net data)"
	provides="hwids-net=20220101-r$pkgrel"
	replaces="hwids-net"

	amove usr/share/hwdata/oui.txt
	amove usr/share/hwdata/iab.txt
}

pnp() {
	pkgdesc="$pkgdesc (pnp data)"

	amove usr/share/hwdata/pnp.ids
}


sha512sums="
63ea6b56283b248a54b4a9e350da431db4f40b30e4d1e9afb29bfec3f87b11723899397817435e9142b7775690a399591afb9b701101e2cc4d78893fa791e1b7  hwdata-0.362.tar.gz
78135dcdda78095ecd5e984ebd1a46f62f4199db4b0293f6654b28ac0c03af78927e1fd05d96674577be31590c6b131e094ca478963f93dbe3a6cb35114f3ba2  install-T.patch
"
